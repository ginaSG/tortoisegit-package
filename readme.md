## 简介

- php环境下的差异文件打包 

- 可用于生成差异补丁

- 将日志信息及文件变动信息生成xml文件

- 生成文件为zip格式

## 说明

- 智能记忆,打开后还原上次内容

- 补丁分支可定义 开发版|稳定版 标识

- 补丁类型可定义 安装包|补丁包 标识
 
- 自定义更新信息

- 根据日志生成XML文件记录

- 实际处理删除文件请读取XML后自行处理

## 使用

### 比较差异

![输入图片说明](https://images.gitee.com/uploads/images/2019/1226/225346_f0094fd3_60481.png "gpk1.png")

### 导出文件

- 导出文件保留了目录格式放入指定目录`src/install` | `src/upgrade`

![输入图片说明](https://images.gitee.com/uploads/images/2019/1226/225414_78571cb1_60481.png "gpk2.png")

### 复制文件变动信息

- 这里复制的信息将用于生成xml文件变更记录

![输入图片说明](https://images.gitee.com/uploads/images/2019/1226/230538_9f3d30a6_60481.png "gpk4.png")

### 生成补丁



![输入图片说明](https://images.gitee.com/uploads/images/2019/1226/225929_cb563e4a_60481.png "gpk3.png")